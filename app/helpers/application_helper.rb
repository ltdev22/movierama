module ApplicationHelper

    def nav_item(path)
        if current_page?(path)
            "active"
        else
            "nav-item"
        end
    end

    def errors_for object
        render :partial => "application/validation_errors", :locals => { :object => object }
    end
end
