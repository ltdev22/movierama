class Movie < ActiveRecord::Base

    # Relationships
    belongs_to :user
    has_many :reviews

    has_attached_file :image, styles: { medium: "400x600#" }
    validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

    # Validations
    validates :title,
                presence: true,
                length: { minimum: 5 }

    validates :description,
                presence: true,
                length: { minimum: 5 }

    validates :year,
                presence: true

    validates :duration,
                presence: true

    validates :rating,
                presence: true
end
