class Review < ActiveRecord::Base

    # Relationships
    belongs_to :user
    belongs_to :movie

    # Validations
    validates :comment,
                presence: true,
                length: { minimum: 5 }

end